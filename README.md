Create a .npmrc from the template file and populate the created file with the values.
To run in local mode, run `npm install`, then, `node index`. Backend will be up in `http://localhost:3000`.
To run docker container in standalone mode, run the commands `npm run image-build` and `npm run image-run`. Backend will be up in `http://localhost:3000`.

To push the docker image to AWS repo, run the commands `npm run image-build`, `npm run image-tag` and `npm run image-push`. 
