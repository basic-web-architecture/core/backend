const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const port = 3000;

app.use(bodyParser.urlencoded({ extended: false }));
app.use((req, res, next) => {
  const allowedOrigins = [ 'http://localhost:4200', process.env.origin || "http://localhost" ];
  const origin = req.headers.origin;
  console.log(origin);
  if (allowedOrigins.includes(origin)) {
       res.setHeader('Access-Control-Allow-Origin', origin);
  }
  // res.header('Access-Control-Allow-Origin', 'http://127.0.0.1:8020');
  // res.header('Access-Control-Allow-Methods', 'GET, OPTIONS');
  // res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');
  // res.header('Access-Control-Allow-Credentials', true);
  return next();
});

app.get('/', (req, res) => {
  res.send('This is the homepage');
});
app.get('/alerts', (req, res) => {
  res.json({description:"Mirror faulty"});
});

app.listen(port, () => {
  console.log(`Success! The application is running on port ${port}.`);
});