FROM node:16-alpine as build
ARG origin_default='http://localhost'
ENV origin=$origin_default
WORKDIR /server
COPY package*.json ./
RUN npm install --only=production   
COPY . .
EXPOSE 3000
ENTRYPOINT ["node", "index.js"]